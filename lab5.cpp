/*
Paul Marcella
pmarcel
Lab 5
Lab Section 001
Nushrat Humaira
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   //This array is used to store the deck
   Card cardArray[52];


  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   int k = 0;
   //The nested for loop is used to make 52 cards, with suits 1-3 and values 2-14
   for(int i = 0; i <= 3; ++i){
     for(int j = 2; j <= 14; ++j){
       cardArray[k].suit = static_cast<Suit>(i);
       cardArray[k].value = j;
       k++;
     }
   }

   //Shuffles all 52 cards randomly
   random_shuffle(&cardArray[0], &cardArray[52], myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //This array stores the first 5 cards of the shuffled deck, making a hand
    Card handCards[5] = { cardArray[0], cardArray[1], cardArray[2], cardArray[3], cardArray[4] };


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     sort(&handCards[0], &handCards[5] , suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

    cout << setw(10) << get_card_name(handCards[0]) << " of " << get_suit_code(handCards[0]) << endl;
    cout << setw(10) << get_card_name(handCards[1]) << " of " << get_suit_code(handCards[1]) << endl;
    cout << setw(10) << get_card_name(handCards[2]) << " of " << get_suit_code(handCards[2]) << endl;
    cout << setw(10) << get_card_name(handCards[3]) << " of " << get_suit_code(handCards[3]) << endl;
    cout << setw(10) << get_card_name(handCards[4]) << " of " << get_suit_code(handCards[4]) << endl;






  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
//Compares if the left card's suit has a lower value than the right's
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit){
    return true;
  }
  //If the suits are the same, the values are compared instead
  else if (lhs.suit == rhs.suit){
    if(lhs.value < rhs.value){
      return true;
    }

  }
  else
  {
    return false;
  }

  return false;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  //Returns a value for numbers and the name of face cards for higher values
  switch (c.value) {
    case 2:   return "2";
    case 3:   return "3";
    case 4:   return "4";
    case 5:   return "5";
    case 6:   return "6";
    case 7:   return "7";
    case 8:   return "8";
    case 9:   return "9";
    case 10:  return "10";
    case 11:  return "Jack";
    case 12:  return "Queen";
    case 13:  return "King";
    case 14:  return "Ace";
    default:  return "";
  }
}
